# M2i3App Commands

A quick series of commands to help manage M2i3 Cloud servers

## Requirements

1. Ubuntu 12.04 or Ubuntu 13.10
2. Progrium PluginHook - https://s3.amazonaws.com/progrium-pluginhook/pluginhook_0.1.0_amd64.deb
3. Docker - https://www.docker.io